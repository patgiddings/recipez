'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Unit = function () {
  function Unit(init) {
    _classCallCheck(this, Unit);

    // type can be: 0->unassigned, 1->count, 2->weight, 3->volume
    this._type = init && init.type ? init.type : 0;
    this._name = init && init.name ? init.name : 'Unassigned';
    this._abbrev = init && init.abbrev ? init.abbrev : '';
    this._matcher = init && init.matcher ? init.matcher : /\w\b\w/;
    // factor is in relation to SI base unit (ea, g, ml)
    this._factor = init && init.factor ? init.factor : 1;
    this._next = init && init.next ? init.next : '';
    this._prev = init && init.prev ? init.prev : '';
  }

  _createClass(Unit, [{
    key: '_copy',
    value: function _copy(b) {
      this._type = b.type;
      this._name = b.name;
      this._abbrev = b.abbrev;
      this._matcher = b.matcher;
      this._factor = b.factor;
      this._next = b.next;
      this._prev = b.prev;
    }
  }, {
    key: 'type',
    get: function get() {
      return this._type;
    },
    set: function set(type) {
      this._type = type;
    }
  }, {
    key: 'name',
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      var unit = UnitMatcher.match(name);
      if (unit) {
        this._copy(unit);
      }
    }
  }, {
    key: 'abbrev',
    get: function get() {
      return this._abbrev;
    },
    set: function set(abbrev) {
      this._abbrev = abbrev;
    }
  }, {
    key: 'matcher',
    get: function get() {
      return this._matcher;
    },
    set: function set(matcher) {
      this._matcher = matcher;
    }
  }, {
    key: 'factor',
    get: function get() {
      return this._factor;
    },
    set: function set(factor) {
      this._factor = factor;
    }
  }, {
    key: 'next',
    get: function get() {
      return this._next;
    },
    set: function set(next) {
      this._next = next;
    }
  }, {
    key: 'prev',
    get: function get() {
      return this._prev;
    },
    set: function set(prev) {
      this._prev = prev;
    }
  }]);

  return Unit;
}();

;
var Bunch = function (_Unit) {
  _inherits(Bunch, _Unit);

  function Bunch() {
    _classCallCheck(this, Bunch);

    return _possibleConstructorReturn(this, (Bunch.__proto__ || Object.getPrototypeOf(Bunch)).call(this, {
      type: 1,
      name: 'bunch',
      abbrev: 'bu',
      matcher: /\bbu\b|bunch/,
      factor: 1
    }));
  }

  return Bunch;
}(Unit);

;
var Count = function (_Unit2) {
  _inherits(Count, _Unit2);

  function Count() {
    _classCallCheck(this, Count);

    return _possibleConstructorReturn(this, (Count.__proto__ || Object.getPrototypeOf(Count)).call(this, {
      type: 1,
      name: 'count',
      abbrev: 'ct',
      matcher: /\bct\b|\bct\.\b|count/,
      factor: 1,
      next: 'dz'
    }));
  }

  return Count;
}(Unit);

;
var Dozen = function (_Unit3) {
  _inherits(Dozen, _Unit3);

  function Dozen() {
    _classCallCheck(this, Dozen);

    return _possibleConstructorReturn(this, (Dozen.__proto__ || Object.getPrototypeOf(Dozen)).call(this, {
      type: 1,
      name: 'dozen',
      abbrev: 'dz',
      matcher: /\bdz\b|dozen/,
      factor: 12,
      prev: 'ct'
    }));
  }

  return Dozen;
}(Unit);

;
var Each = function (_Unit4) {
  _inherits(Each, _Unit4);

  function Each() {
    _classCallCheck(this, Each);

    return _possibleConstructorReturn(this, (Each.__proto__ || Object.getPrototypeOf(Each)).call(this, {
      type: 1,
      name: 'each',
      abbrev: 'ea',
      matcher: /\bea\b|\bea\.\b|each/,
      factor: 1,
      next: 'dz'
    }));
  }

  return Each;
}(Unit);

;
var UnitMatcher = function () {
  function UnitMatcher() {
    _classCallCheck(this, UnitMatcher);
  }

  _createClass(UnitMatcher, null, [{
    key: 'match',
    value: function match(unit) {
      var units = UnitMatcher.units;
      for (var i = 0; i < units.length; i++) {
        if (unit.search(units[i].matcher) >= 0) {
          return units[i];
        }
      }
    }
  }, {
    key: 'units',
    get: function get() {
      return [new Each(), new Count(), new Bunch(), new Dozen(), new FluidOunce(), new Teaspoon(), new Tablespoon(), new Cup(), new Pint(), new Quart(), new Gallon(), new Ounce(), new Pound(), new Mililiter(), new Liter(), new Gram(), new Kilogram()];
    }
  }]);

  return UnitMatcher;
}();

;
var Cup = function (_Unit5) {
  _inherits(Cup, _Unit5);

  function Cup() {
    _classCallCheck(this, Cup);

    return _possibleConstructorReturn(this, (Cup.__proto__ || Object.getPrototypeOf(Cup)).call(this, {
      type: 3,
      name: 'cup',
      abbrev: 'c',
      matcher: /\bc\b|cup|\bcp\b/,
      factor: 236.5882365,
      next: 'pt',
      prev: 'fl oz'
    }));
  }

  return Cup;
}(Unit);

;
var FluidOunce = function (_Unit6) {
  _inherits(FluidOunce, _Unit6);

  function FluidOunce() {
    _classCallCheck(this, FluidOunce);

    return _possibleConstructorReturn(this, (FluidOunce.__proto__ || Object.getPrototypeOf(FluidOunce)).call(this, {
      type: 3,
      name: 'fluid ounce',
      abbrev: 'fl oz',
      matcher: /fl\.\soz|fluid\sounce|fluid\soz|fl\soz/,
      factor: 29.5735295625,
      next: 'cup',
      prev: 'tbl'
    }));
  }

  return FluidOunce;
}(Unit);

;
var Gallon = function (_Unit7) {
  _inherits(Gallon, _Unit7);

  function Gallon() {
    _classCallCheck(this, Gallon);

    return _possibleConstructorReturn(this, (Gallon.__proto__ || Object.getPrototypeOf(Gallon)).call(this, {
      type: 3,
      name: 'gallon',
      abbrev: 'gal',
      matcher: /gal|gallon/,
      factor: 3785.411784,
      prev: 'qt'
    }));
  }

  return Gallon;
}(Unit);

;
var Pint = function (_Unit8) {
  _inherits(Pint, _Unit8);

  function Pint() {
    _classCallCheck(this, Pint);

    return _possibleConstructorReturn(this, (Pint.__proto__ || Object.getPrototypeOf(Pint)).call(this, {
      type: 3,
      name: 'pint',
      abbrev: 'pt',
      matcher: /\bpt\b|pint/,
      factor: 473.176473,
      next: 'qt',
      prev: 'c'
    }));
  }

  return Pint;
}(Unit);

;
var Quart = function (_Unit9) {
  _inherits(Quart, _Unit9);

  function Quart() {
    _classCallCheck(this, Quart);

    return _possibleConstructorReturn(this, (Quart.__proto__ || Object.getPrototypeOf(Quart)).call(this, {
      type: 3,
      name: 'quart',
      abbrev: 'qt',
      matcher: /\bqt\b|quart/,
      factor: 946.352946,
      next: 'gal',
      prev: 'pt'
    }));
  }

  return Quart;
}(Unit);

;
var Tablespoon = function (_Unit10) {
  _inherits(Tablespoon, _Unit10);

  function Tablespoon() {
    _classCallCheck(this, Tablespoon);

    return _possibleConstructorReturn(this, (Tablespoon.__proto__ || Object.getPrototypeOf(Tablespoon)).call(this, {
      type: 3,
      name: 'tablespoon',
      abbrev: 'tbl',
      matcher: /tbl|tablespoon|\btb\b/,
      factor: 14.78676478125,
      next: 'cup',
      prev: 'tsp'
    }));
  }

  return Tablespoon;
}(Unit);

;
var Teaspoon = function (_Unit11) {
  _inherits(Teaspoon, _Unit11);

  function Teaspoon() {
    _classCallCheck(this, Teaspoon);

    return _possibleConstructorReturn(this, (Teaspoon.__proto__ || Object.getPrototypeOf(Teaspoon)).call(this, {
      type: 3,
      name: 'teaspoon',
      abbrev: 'tsp',
      matcher: /tsp|teaspoon|\bts\b/,
      factor: 4.92892159375,
      next: 'tbl'
    }));
  }

  return Teaspoon;
}(Unit);

;
var Liter = function (_Unit12) {
  _inherits(Liter, _Unit12);

  function Liter() {
    _classCallCheck(this, Liter);

    return _possibleConstructorReturn(this, (Liter.__proto__ || Object.getPrototypeOf(Liter)).call(this, {
      type: 3,
      name: 'liter',
      abbrev: 'lt',
      matcher: /\blt\b|liter|\bl\b/,
      factor: 1000,
      prev: 'ml'
    }));
  }

  return Liter;
}(Unit);

;
var Mililiter = function (_Unit13) {
  _inherits(Mililiter, _Unit13);

  function Mililiter() {
    _classCallCheck(this, Mililiter);

    return _possibleConstructorReturn(this, (Mililiter.__proto__ || Object.getPrototypeOf(Mililiter)).call(this, {
      type: 3,
      name: 'mililiter',
      abbrev: 'ml',
      matcher: /\bml\|mililiter|mili\b/,
      factor: 1,
      next: 'lt'
    }));
  }

  return Mililiter;
}(Unit);

;
var Ounce = function (_Unit14) {
  _inherits(Ounce, _Unit14);

  function Ounce() {
    _classCallCheck(this, Ounce);

    return _possibleConstructorReturn(this, (Ounce.__proto__ || Object.getPrototypeOf(Ounce)).call(this, {
      type: 2,
      name: 'ounce',
      abbrev: 'oz',
      matcher: /\boz\b|\boz\.\b|ounce/,
      factor: 28.349523125,
      next: 'lb'
    }));
  }

  return Ounce;
}(Unit);

;
var Pound = function (_Unit15) {
  _inherits(Pound, _Unit15);

  function Pound() {
    _classCallCheck(this, Pound);

    return _possibleConstructorReturn(this, (Pound.__proto__ || Object.getPrototypeOf(Pound)).call(this, {
      type: 2,
      name: 'pound',
      abbrev: 'lb',
      matcher: /\blb\b|\blb\.\b|pound/,
      factor: 453.59237,
      prev: 'oz'
    }));
  }

  return Pound;
}(Unit);

;
var Gram = function (_Unit16) {
  _inherits(Gram, _Unit16);

  function Gram() {
    _classCallCheck(this, Gram);

    return _possibleConstructorReturn(this, (Gram.__proto__ || Object.getPrototypeOf(Gram)).call(this, {
      type: 2,
      name: 'gram',
      abbrev: 'g',
      matcher: /\bg\b|gram/,
      factor: 1
    }));
  }

  return Gram;
}(Unit);

;
var Kilogram = function (_Unit17) {
  _inherits(Kilogram, _Unit17);

  function Kilogram() {
    _classCallCheck(this, Kilogram);

    return _possibleConstructorReturn(this, (Kilogram.__proto__ || Object.getPrototypeOf(Kilogram)).call(this, {
      type: 2,
      name: 'kilogram',
      abbrev: 'kg',
      matcher: /\bkg\b|kilo\b|kilogram/,
      factor: 1000
    }));
  }

  return Kilogram;
}(Unit);

;angular.module('recipez', []).controller('welcome', ['$scope', '$timeout', function ($scope, $timeout) {
  var self = this;

  self.positionCount = 2;
  self.slideListeners = [];
  self.about = ['What is Recipease?', 'Tell me about the recipe.', 'What goes into it?'];
  self.recipe = new Recipe('Fresh salsa', 'Pico de Gallo', new Quantity(3, 'qt'), [new Ingredient('Lime', new Quantity(2, 'ea'), 'juice and zest', 0), new Ingredient('Cilantro', new Quantity(.5, 'bu'), 'chopped', 10)]);
  // self.recipe = new Recipe();

  self.ingredient = new Ingredient();

  $scope.state = {
    position: 2,
    positions: 4
  };

  $scope.onSlide = function (listener) {
    self.slideListeners.push(listener);
    listener($scope.state.position);
  };

  self.notify = function () {
    self.slideListeners.forEach(function (f) {
      f($scope.state.position);
    });
  };

  self.slideUp = function () {
    if ($scope.state.position >= $scope.state.positions - 1) {
      return;
    }

    $scope.state.position++;
    self.notify();
  };

  self.slideDown = function () {
    if ($scope.state.position <= 0) {
      return;
    }

    $scope.state.position--;
    self.notify();
  };

  self.addIngredient = function () {
    angular.element('#ingredient-name :input  ')[0].focus();
    if (self.ingredient.name.length === 0) {
      return;
    }
    $timeout(function () {
      self.recipe.addIngredient(self.ingredient);
      self.ingredient = new Ingredient();
    }, 200);
  };

  self.parseQuantity = function (qty) {
    // if (qty && qty.includes) {
    //   if (qty.includes('/')) {
    //     const fraction = qty.split('/');
    //     if (fraction.length !== 2) {
    //       self.ingredient.quantity.quantity = 0;
    //     } else {
    //       const decimal = parseFloat(fraction[0]) / parseFloat(fraction[1]);
    //       self.ingredient.quantity.quantity = decimal;
    //     }
    //   } else {
    //     self.ingredient.quantity.quantity = parseFloat(qty);
    //   }
    // } else {
    //   self.ingredient.quantity.quantity = 0;
    // }
  };

  self.popIngredient = function (index) {
    self.ingredient = self.recipe.ingredients[index];
    self.recipe.ingredients.splice(index, 1);
  };

  self.scale = function () {
    self.recipe.scale(.5);
  };

  self.submit = function () {
    console.log(['Submit Recipe', self.recipe]);
  };
}]);
;angular.module('recipez').directive('slide', [function () {
  return {
    link: function link(scope, element, attrs) {
      element._position = 0;
      function slide(position) {
        element.removeClass('pos-' + element._position);
        element._position = position;
        element.addClass('pos-' + position);
      }
      scope.onSlide(slide);
    }
  };
}]).directive('formGroup', ['$parse', function ($parse) {
  var template = "<div class='form_group'>" + "<label for='{{name | lowercase}}' ng-click='toggle($event);'>{{name}}</label>" + "<input type='text' name='{{name | lowercase}}' ng-model='innerModel' ng-focus='focus($event);' ng-blur='focus($event, true);' ng-keypress='checkForEnter($event);'>" + "</div>";

  function link(scope, element, attrs) {
    scope.innerModel = scope.model || '';
    scope.$watch('model', function (m) {
      scope.innerModel = m || '';
    });
    if ('down' in attrs) {
      element.addClass('down');
    }
    scope.focus = function (e, blur) {
      var el = angular.element(e.target);
      if (blur) {
        scope.model = scope.innerModel;
        el.removeClass('focused');
        el.prev().removeClass('focused');
        'down' in attrs && el.prev().removeClass('down');
        scope.onBlur && scope.onBlur({ qty: scope.model });
      } else {
        el.addClass('focused');
        el.prev().addClass('focused');
        'down' in attrs && el.prev().addClass('down');
      }
    };
    scope.checkForEnter = function (e) {
      if (e.which === 13) {
        angular.element(e.target).blur();
        if ('onEnter' in attrs) {
          scope.onEnter();
        }
      }
    };
    scope.toggle = function (e) {
      var el = angular.element(e.target).next();
      if (el.hasClass('focused')) {
        el.blur();
      } else {
        el.focus();
      }
    };
  }

  return {
    restrict: 'E',
    replace: true,
    transclude: false,
    scope: {
      model: "=",
      name: "@",
      onEnter: "&?",
      onBlur: "&?",
      noShadow: "=?"
    },
    link: link,
    template: template
  };
}]);
var Fraction = function () {
  function Fraction(dec) {
    _classCallCheck(this, Fraction);

    this._decimal = dec;
    var decimalArray = ("" + dec).split(".");
    if (decimalArray.length > 1) {
      var leftDecimalPart = decimalArray[0];
      var rightDecimalPart = decimalArray[1].substring(0, 4);

      var numerator = leftDecimalPart + rightDecimalPart;
      var denominator = Math.pow(10, rightDecimalPart.length);
      var factor = this._highestCommonFactor(numerator, denominator);
      this._denominator = denominator / factor;
      this._numerator = numerator / factor;
    } else {
      this._denominator = 1;
      this._numerator = decimalArray[0];
    }
  }

  _createClass(Fraction, [{
    key: '_highestCommonFactor',
    value: function _highestCommonFactor(a, b) {
      if (b == 0) return a;
      return this._highestCommonFactor(b, a % b);
    }
  }, {
    key: 'toString',
    value: function toString() {
      if (this._denominator === 1) {
        return this._numerator;
      } else if (this._numerator > this._denominator) {
        var dividend = Math.floor(this._numerator / this._denominator);
        var remainder = this._numerator % this._denominator;
        return dividend + ' ' + remainder + '/' + this._denominator;
      } else {
        return this._numerator + '/' + this._denominator;
      }
    }
  }, {
    key: 'decimal',
    get: function get() {
      return this._decimal;
    }
  }, {
    key: 'numerator',
    get: function get() {
      return this._numerator;
    }
  }, {
    key: 'denominator',
    get: function get() {
      return this._denominator;
    }
  }]);

  return Fraction;
}();

;
var Ingredient = function () {
  function Ingredient(name, quantity, method, id) {
    _classCallCheck(this, Ingredient);

    this._id = id || 0;
    this._name = name || '';
    this._method = method || '';
    this._quantity = quantity || new Quantity();
  }

  _createClass(Ingredient, [{
    key: 'scale',
    value: function scale(factor) {
      this._quantity.scale(factor);
      return this;
    }
  }, {
    key: 'toString',
    value: function toString() {
      return this._quantity + ' ' + this._name;
    }
  }, {
    key: 'id',
    get: function get() {
      return this._id;
    },
    set: function set(id) {
      this._id = id;
    }
  }, {
    key: 'name',
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: 'method',
    get: function get() {
      return this._method;
    },
    set: function set(method) {
      this._method = method;
    }
  }, {
    key: 'quantity',
    get: function get() {
      return this._quantity;
    },
    set: function set(quantity) {
      this._quantity = quantity;
    }
  }]);

  return Ingredient;
}();

;
var Quantity = function () {
  function Quantity(quantity, unit) {
    _classCallCheck(this, Quantity);

    this._quantity = quantity || 0;
    this._fraction = new Fraction(this._quantity);
    this._unit = unit && unit.name ? unit : unit ? UnitMatcher.match(unit) : UnitMatcher.match('ea');
  }

  _createClass(Quantity, [{
    key: 'simplify',
    value: function simplify() {
      var newQuantity = new Quantity(this._quantity, this._unit);
      console.log('simplify: ', this);
      if (this._quantity < 1) {
        // simplify down
        while (newQuantity.quantity.decimal < 1 && newQuantity.unit.prev) {
          var unit = UnitMatcher.match(newQuantity.unit.prev);
          var quantity = Math.round(newQuantity.quantity.decimal * (newQuantity.unit.factor / unit.factor) * 100000) / 100000;
          if ('123468'.includes(new Fraction(quantity).denominator + '')) {
            newQuantity = new Quantity(quantity, unit);
          } else {
            break;
          }
        }
      } else {
        // simplify up
        while (newQuantity.unit.next && newQuantity.unit.next.length > 0) {
          var _unit = UnitMatcher.match(newQuantity.unit.next);
          var _quantity = Math.round(newQuantity.quantity.decimal * (newQuantity.unit.factor / _unit.factor) * 100000) / 100000;
          var remainder = _quantity % .25;
          if (remainder === 0) {
            newQuantity = new Quantity(_quantity, _unit);
          } else {
            break;
          }
        }
      }

      this.quantity = newQuantity.quantity;
      this.unit = newQuantity.unit;
      return this;
    }
  }, {
    key: 'scale',
    value: function scale(factor) {
      this._quantity *= factor;
      this._fraction = new Fraction(this._quantity);
      this.simplify();
      return this;
    }
  }, {
    key: 'toDecimalString',
    value: function toDecimalString() {
      return this._quantity + ' ' + this._unit.name + this._pluralize();
    }
  }, {
    key: 'toString',
    value: function toString() {
      return new Fraction(this._quantity) + ' ' + this._unit.name + this._pluralize();
    }
  }, {
    key: '_pluralize',
    value: function _pluralize() {
      return this._quantity > 1 && this._unit.type > 1 ? 's' : '';
    }
  }, {
    key: 'quantity',
    get: function get() {
      return this._fraction;
    },
    set: function set(quantity) {
      var newQuantity = {};
      if (quantity) {
        if (quantity.includes) {
          if (quantity.includes('/')) {
            var fraction = quantity.split('/');
            if (fraction.length !== 2) {
              newQuantity._quantity = void 0;
            } else {
              var decimal = parseFloat(fraction[0]) / parseFloat(fraction[1]);
              newQuantity._quantity = decimal;
              newQuantity._fraction = new Fraction(decimal);
            }
          } else {
            newQuantity._quantity = parseFloat(quantity);
            newQuantity._fraction = new Fraction(newQuantity._quantity);
          }
        } else if (quantity.denominator) {
          newQuantity._fraction = quantity;
          newQuantity._quantity = quantity.decimal;
        } else {
          newQuantity._quantity = parseFloat(quantity);
          this._fraction = new Fraction(this._quantity);
        }
      }

      if (newQuantity._quantity !== undefined && newQuantity._quantity !== NaN) {
        this._quantity = newQuantity._quantity;
        this._fraction = newQuantity._fraction;
      }
    }
  }, {
    key: 'unit',
    get: function get() {
      return this._unit;
    },
    set: function set(unit) {
      this._unit = unit;
    }
  }]);

  return Quantity;
}();

;
var Recipe = function () {
  function Recipe(description, name, rYield, ingredients) {
    _classCallCheck(this, Recipe);

    this._description = description || '';
    this._name = name || '';
    this._yield = rYield || new Quantity(0, 'ea');

    if (ingredients && ingredients.length > 0) {
      var ids = ingredients.map(function (ingredient) {
        return ingredient.id;
      });
      var lastId = Math.max.apply(Math, _toConsumableArray(ids));
      this._ingredients = ingredients;
      this._nextId = lastId + 1;
    } else {
      this._ingredients = [];
      this._nextId = 0;
    }
  }

  _createClass(Recipe, [{
    key: 'addIngredient',
    value: function addIngredient(ingredient) {
      if (ingredient) {
        ingredient.id = this._nextId;
        ingredient.quantity.simplify();
        this._nextId++;
        this._ingredients.push(ingredient);
      }
    }
  }, {
    key: 'scale',
    value: function scale(factor) {
      if (factor > 0) {
        this._yield.scale(factor);
        this._ingredients.forEach(function (i) {
          return i.scale(factor);
        });
      }
    }
  }, {
    key: 'description',
    get: function get() {
      return this._description;
    },
    set: function set(description) {
      this._description = description;
    }
  }, {
    key: 'name',
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: 'yield',
    get: function get() {
      return this._yield;
    },
    set: function set(rYield) {
      this._yield = rYield;
    }
  }, {
    key: 'ingredients',
    get: function get() {
      return this._ingredients;
    },
    set: function set(ingredients) {
      this._ingredients = ingredients;
    }
  }]);

  return Recipe;
}();
