module.exports = function(grunt) {

  grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            build: ['Gruntfile.js', 'src/main/js/**/*.js']
        },
        babel: {
            options: {
                compact: false
            },
            files: {
                src: [
                    'src/main/js/app.es6'
                ],
                dest: 'dist/main/js/app.js'
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            imports: {
                src: [
                    'src/main/imports/jquery/*.js',
                    'src/main/imports/angular/*.js',
                    'src/main/imports/react/*.js',
                    'src/main/imports/react-dom/*.js'
                ],
                dest: 'src/main/imports/imports.es6'
            },
            build: {
                src: [
                    'src/main/js/units/unit.class.js',
                    'src/main/js/units/**/*.js',
                    'src/main/js/*.js',
                    'src/main/js/**/*.js'
                ],
                dest: 'src/main/js/app.es6'
            }
        },
        uglify: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            build: {
                files: {
                    'dist/main/js/app.min.js': 'dist/main/js/app.js'

                }
            },
            imports: {
                files: {
                    'dist/main/js/imports.min.js': 'src/main/imports/imports.es6'
                }
            }
        },
        less: {
            build: {
                files: {
                    'dist/main/style/app.css': ['src/main/style/**/*.less']
                }
            }
        },
        cssmin: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            build: {
                files: {
                    'dist/main/style/app.min.css': 'dist/main/style/app.css'
                }
            }
        },
        watch: {
            stylesheets: {
                files: ['src/main/style/**/*.css', 'src/main/style/**/*.less'],
                tasks: ['less', 'cssmin']
            },
            scripts: {
                files: ['src/main/js/**/*.js'],
                tasks: ['concat:build', 'babel', 'uglify:build']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-babel');
    grunt.registerTask('default', ['uglify', 'less', 'cssmin']);
};
