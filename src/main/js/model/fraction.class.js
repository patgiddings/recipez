class Fraction {

  constructor(dec) {
    this._decimal = dec;
    const decimalArray = ("" + dec).split(".");
    if (decimalArray.length > 1) {
      const leftDecimalPart = decimalArray[0];
      const rightDecimalPart = decimalArray[1].substring(0, 4);

      const numerator = leftDecimalPart + rightDecimalPart
      const denominator = Math.pow(10, rightDecimalPart.length);
      const factor = this._highestCommonFactor(numerator, denominator);
      this._denominator = denominator / factor;
      this._numerator = numerator / factor;
    } else {
      this._denominator = 1;
      this._numerator = decimalArray[0];
    }
  }

  get decimal() {
    return this._decimal;
  }

  get numerator() {
    return this._numerator;
  }

  get denominator() {
    return this._denominator;
  }


  _highestCommonFactor(a,b) {
      if (b==0) return a;
      return this._highestCommonFactor(b,a%b);
  }

  toString() {
    if (this._denominator === 1) {
      return this._numerator;
    } else if (this._numerator > this._denominator) {
      const dividend = Math.floor(this._numerator / this._denominator);
      const remainder = this._numerator % this._denominator;
      return dividend + ' ' + remainder + '/' + this._denominator;
    } else {
      return this._numerator + '/' + this._denominator;
    }
  }

}
