class Quantity {
  constructor(quantity, unit) {
    this._quantity = quantity || 0;
    this._fraction = new Fraction(this._quantity);
    this._unit = unit && unit.name ? unit : (unit ? UnitMatcher.match(unit) : UnitMatcher.match('ea'));
  }

  get quantity() {
    return this._fraction;
  }

  set quantity(quantity) {
    let newQuantity = {};
    if (quantity) {
      if (quantity.includes) {
        if (quantity.includes('/')) {
          const fraction = quantity.split('/');
          if (fraction.length !==2) {
            newQuantity._quantity = void 0;
          } else {
            const decimal = parseFloat(fraction[0]) / parseFloat(fraction[1]);
            newQuantity._quantity = decimal;
            newQuantity._fraction = new Fraction(decimal);
          }
        } else {
          newQuantity._quantity = parseFloat(quantity);
          newQuantity._fraction = new Fraction(newQuantity._quantity);
        }
      } else if (quantity.denominator) {
        newQuantity._fraction = quantity;
        newQuantity._quantity = quantity.decimal;
      } else {
        newQuantity._quantity = parseFloat(quantity);
        this._fraction = new Fraction(this._quantity);
      }
    }

    if (newQuantity._quantity !== undefined && newQuantity._quantity !== NaN) {
      this._quantity = newQuantity._quantity;
      this._fraction = newQuantity._fraction;
    }
  }

  get unit() {
    return this._unit;
  }

  set unit(unit) {
    this._unit = unit;
  }

  simplify() {
    let newQuantity = new Quantity(this._quantity, this._unit);
    console.log('simplify: ', this);
    if (this._quantity < 1) {
      // simplify down
      while (newQuantity.quantity.decimal < 1 && newQuantity.unit.prev) {
        const unit = UnitMatcher.match(newQuantity.unit.prev);
        const quantity = Math.round(newQuantity.quantity.decimal * (newQuantity.unit.factor / unit.factor)*100000)/100000;
        if ('123468'.includes(new Fraction(quantity).denominator + '')) {
          newQuantity = new Quantity(quantity, unit);
        } else {
          break;
        }
      }
    } else {
      // simplify up
      while (newQuantity.unit.next && newQuantity.unit.next.length > 0) {
        const unit = UnitMatcher.match(newQuantity.unit.next);
        const quantity = Math.round(newQuantity.quantity.decimal * (newQuantity.unit.factor / unit.factor)*100000)/100000;
        const remainder = quantity % .25;
        if (remainder === 0) {
          newQuantity = new Quantity(quantity, unit);
        } else {
          break;
        }
      }
    }

    this.quantity = newQuantity.quantity;
    this.unit = newQuantity.unit;
    return this;
  }

  scale(factor) {
    this._quantity *= factor;
    this._fraction = new Fraction(this._quantity);
    this.simplify();
    return this;
  }

  toDecimalString() {
    return this._quantity + ' ' + this._unit.name + this._pluralize();
  }

  toString() {
    return new Fraction(this._quantity) + ' ' + this._unit.name + this._pluralize();
  }

  _pluralize() {
    return (this._quantity > 1 && this._unit.type > 1 ? 's' : '');
  }
}
