class Ingredient {
  constructor(name, quantity, method, id) {
    this._id = id || 0;
    this._name = name || '';
    this._method = method || '';
    this._quantity = quantity || new Quantity();
  }

  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get method() {
    return this._method;
  }

  set method(method) {
    this._method = method;
  }

  get quantity() {
    return this._quantity;
  }

  set quantity(quantity) {
    this._quantity = quantity;
  }

  scale(factor) {
    this._quantity.scale(factor);
    return this;
  }

  toString() {
    return this._quantity + ' ' + this._name;
  }

}
