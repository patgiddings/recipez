class Recipe {
  constructor(description, name, rYield, ingredients) {
    this._description = description || '';
    this._name = name || '';
    this._yield = rYield || new Quantity(0, 'ea');

    if (ingredients && ingredients.length > 0) {
      const ids = ingredients.map(ingredient => ingredient.id);
      const lastId = Math.max(...ids);
      this._ingredients = ingredients;
      this._nextId = lastId + 1;
    } else {
      this._ingredients = [];
      this._nextId = 0;
    }
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get yield() {
    return this._yield;
  }

  set yield(rYield) {
    this._yield = rYield;
  }

  get ingredients() {
    return this._ingredients;
  }

  set ingredients(ingredients) {
    this._ingredients = ingredients;
  }

  addIngredient(ingredient) {
    if (ingredient) {
      ingredient.id = this._nextId;
      ingredient.quantity.simplify();
      this._nextId++;
      this._ingredients.push(ingredient);
    }
  }

  scale(factor) {
    if (factor > 0) {
      this._yield.scale(factor);
      this._ingredients.forEach(i => i.scale(factor));
    }
  }
}
