class Unit {
  constructor(init) {
    // type can be: 0->unassigned, 1->count, 2->weight, 3->volume
    this._type = init && init.type ? init.type : 0;
    this._name = init && init.name ? init.name : 'Unassigned';
    this._abbrev = init && init.abbrev ? init.abbrev : '';
    this._matcher = init && init.matcher ? init.matcher : /\w\b\w/;
    // factor is in relation to SI base unit (ea, g, ml)
    this._factor = init && init.factor ? init.factor : 1;
    this._next = init && init.next ? init.next : '';
    this._prev = init && init.prev ? init.prev : '';
  }

  get type() {
    return this._type;
  }

  set type(type) {
    this._type = type;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    const unit = UnitMatcher.match(name);
    if (unit) {
      this._copy(unit);
    }
  }

  get abbrev() {
    return this._abbrev;
  }

  set abbrev(abbrev) {
    this._abbrev = abbrev;
  }

  get matcher() {
    return this._matcher;
  }

  set matcher(matcher) {
    this._matcher = matcher;
  }

  get factor() {
    return this._factor;
  }

  set factor(factor) {
    this._factor = factor;
  }

  get next() {
    return this._next;
  }

  set next(next) {
    this._next = next;
  }

  get prev() {
    return this._prev;
  }

  set prev(prev) {
    this._prev = prev;
  }

  _copy(b) {
    this._type = b.type;
    this._name = b.name;
    this._abbrev = b.abbrev;
    this._matcher = b.matcher;
    this._factor = b.factor;
    this._next = b.next;
    this._prev = b.prev;
  }
}
;class Bunch extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'bunch',
      abbrev: 'bu',
      matcher: /\bbu\b|bunch/,
      factor: 1
    });
  }
}
;class Count extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'count',
      abbrev: 'ct',
      matcher: /\bct\b|\bct\.\b|count/,
      factor: 1,
      next: 'dz'
    });
  }
}
;class Dozen extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'dozen',
      abbrev: 'dz',
      matcher: /\bdz\b|dozen/,
      factor: 12,
      prev: 'ct'
    });
  }
}
;class Each extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'each',
      abbrev: 'ea',
      matcher: /\bea\b|\bea\.\b|each/,
      factor: 1,
      next: 'dz'
    });
  }
}
;class UnitMatcher {
  static get units() {
    return [
      new Each(),
      new Count(),
      new Bunch(),
      new Dozen(),
      new FluidOunce(),
      new Teaspoon(),
      new Tablespoon(),
      new Cup(),
      new Pint(),
      new Quart(),
      new Gallon(),
      new Ounce(),
      new Pound(),
      new Mililiter(),
      new Liter(),
      new Gram(),
      new Kilogram()
    ];
  }

  static match(unit) {
    let units = UnitMatcher.units;
    for (let i = 0; i < units.length; i++) {
      if (unit.search(units[i].matcher) >= 0) {
        return units[i];
      }
    }
  }

}
;class Cup extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'cup',
      abbrev: 'c',
      matcher: /\bc\b|cup|\bcp\b/,
      factor: 236.5882365,
      next: 'pt',
      prev: 'fl oz'
    });
  }
}
;class FluidOunce extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'fluid ounce',
      abbrev: 'fl oz',
      matcher: /fl\.\soz|fluid\sounce|fluid\soz|fl\soz/,
      factor: 29.5735295625,
      next: 'cup',
      prev: 'tbl'
    });
  }
}
;class Gallon extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'gallon',
      abbrev: 'gal',
      matcher: /gal|gallon/,
      factor: 3785.411784,
      prev: 'qt'
    });
  }
}
;class Pint extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'pint',
      abbrev: 'pt',
      matcher: /\bpt\b|pint/,
      factor: 473.176473,
      next: 'qt',
      prev: 'c'
    });
  }
}
;class Quart extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'quart',
      abbrev: 'qt',
      matcher: /\bqt\b|quart/,
      factor: 946.352946,
      next: 'gal',
      prev: 'pt'
    });
  }
}
;class Tablespoon extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'tablespoon',
      abbrev: 'tbl',
      matcher: /tbl|tablespoon|\btb\b/,
      factor: 14.78676478125,
      next: 'cup',
      prev: 'tsp'
    });
  }
}
;class Teaspoon extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'teaspoon',
      abbrev: 'tsp',
      matcher: /tsp|teaspoon|\bts\b/,
      factor: 4.92892159375,
      next: 'tbl'
    });
  }
}
;class Liter extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'liter',
      abbrev: 'lt',
      matcher: /\blt\b|liter|\bl\b/,
      factor: 1000,
      prev: 'ml'
    });
  }
}
;class Mililiter extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'mililiter',
      abbrev: 'ml',
      matcher: /\bml\|mililiter|mili\b/,
      factor: 1,
      next: 'lt'
    });
  }
}
;class Ounce extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'ounce',
      abbrev: 'oz',
      matcher: /\boz\b|\boz\.\b|ounce/,
      factor: 28.349523125,
      next: 'lb'
    });
  }
}
;class Pound extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'pound',
      abbrev: 'lb',
      matcher: /\blb\b|\blb\.\b|pound/,
      factor: 453.59237,
      prev: 'oz'
    });
  }
}
;class Gram extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'gram',
      abbrev: 'g',
      matcher: /\bg\b|gram/,
      factor: 1
    });
  }
}
;class Kilogram extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'kilogram',
      abbrev: 'kg',
      matcher: /\bkg\b|kilo\b|kilogram/,
      factor: 1000
    });
  }
}
;angular.module('recipez', [])
.controller('welcome', ['$scope', '$timeout', function($scope, $timeout) {
  var self = this;

  self.positionCount = 2;
  self.slideListeners = [];
  self.about = ['What is Recipease?', 'Tell me about the recipe.', 'What goes into it?'];
  self.recipe = new Recipe(
    'Fresh salsa',
    'Pico de Gallo',
    new Quantity(3, 'qt'),
    [
      new Ingredient('Lime', new Quantity(2, 'ea'), 'juice and zest', 0),
      new Ingredient('Cilantro', new Quantity(.5, 'bu'), 'chopped', 10)
    ]
  );
  // self.recipe = new Recipe();

  self.ingredient = new Ingredient();

  $scope.state = {
    position: 2,
    positions: 4
  };

  $scope.onSlide = function(listener) {
    self.slideListeners.push(listener);
    listener($scope.state.position);
  }

  self.notify = function() {
    self.slideListeners.forEach((f) => { f($scope.state.position); });
  }

  self.slideUp = function() {
    if ($scope.state.position >= $scope.state.positions - 1) {
      return;
    }

    $scope.state.position++;
    self.notify();
  }

  self.slideDown = function() {
    if ($scope.state.position <= 0) {
      return;
    }

    $scope.state.position--;
    self.notify();
  }

  self.addIngredient = function() {
    angular.element('#ingredient-name :input  ')[0].focus();
    if (self.ingredient.name.length === 0) {
      return;
    }
    $timeout(function() {
      self.recipe.addIngredient(self.ingredient);
      self.ingredient = new Ingredient();
    }, 200);
  }

  self.parseQuantity = function(qty) {
    // if (qty && qty.includes) {
    //   if (qty.includes('/')) {
    //     const fraction = qty.split('/');
    //     if (fraction.length !== 2) {
    //       self.ingredient.quantity.quantity = 0;
    //     } else {
    //       const decimal = parseFloat(fraction[0]) / parseFloat(fraction[1]);
    //       self.ingredient.quantity.quantity = decimal;
    //     }
    //   } else {
    //     self.ingredient.quantity.quantity = parseFloat(qty);
    //   }
    // } else {
    //   self.ingredient.quantity.quantity = 0;
    // }
  }

  self.popIngredient = function(index) {
    self.ingredient = self.recipe.ingredients[index];
    self.recipe.ingredients.splice(index, 1);
  }

  self.scale = function() {
    self.recipe.scale(.5);
  }

  self.submit = function() {
    console.log(['Submit Recipe', self.recipe]);
  }

}]);
;angular.module('recipez').directive('slide', [function() {
  return {
    link: function(scope, element, attrs) {
      element._position = 0;
      function slide(position) {
          element.removeClass('pos-' + element._position);
          element._position = position;
          element.addClass('pos-' + position);
      }
      scope.onSlide(slide);
    }
  }
}])
.directive('formGroup', ['$parse', function($parse) {
  let template =
    "<div class='form_group'>" +
      "<label for='{{name | lowercase}}' ng-click='toggle($event);'>{{name}}</label>" +
      "<input type='text' name='{{name | lowercase}}' ng-model='innerModel' ng-focus='focus($event);' ng-blur='focus($event, true);' ng-keypress='checkForEnter($event);'>" +
    "</div>";

  function link(scope, element, attrs) {
    scope.innerModel = scope.model || '';
    scope.$watch('model', (m) => { scope.innerModel = m || '';});
    if ('down' in attrs) {
      element.addClass('down');
    }
    scope.focus = function(e, blur) {
      const el = angular.element(e.target);
      if (blur) {
        scope.model = scope.innerModel;
        el.removeClass('focused');
        el.prev().removeClass('focused');
        ('down' in attrs) && el.prev().removeClass('down');
        (scope.onBlur) && scope.onBlur({qty: scope.model});
      } else {
        el.addClass('focused');
        el.prev().addClass('focused');
        ('down' in attrs) && el.prev().addClass('down');
      }
    };
    scope.checkForEnter = function(e) {
      if (e.which === 13) {
        angular.element(e.target).blur();
        if ('onEnter' in attrs) {
          scope.onEnter();
        }
      }
    };
    scope.toggle = function(e) {
      const el = angular.element(e.target).next();
      if (el.hasClass('focused')) {
        el.blur();
      } else {
        el.focus();
      }
    }
  }

  return {
    restrict: 'E',
    replace: true,
    transclude: false,
    scope: {
      model: "=",
      name: "@",
      onEnter: "&?",
      onBlur: "&?",
      noShadow: "=?"
    },
    link: link,
    template: template
  };
}])
;class Fraction {

  constructor(dec) {
    this._decimal = dec;
    const decimalArray = ("" + dec).split(".");
    if (decimalArray.length > 1) {
      const leftDecimalPart = decimalArray[0];
      const rightDecimalPart = decimalArray[1].substring(0, 4);

      const numerator = leftDecimalPart + rightDecimalPart
      const denominator = Math.pow(10, rightDecimalPart.length);
      const factor = this._highestCommonFactor(numerator, denominator);
      this._denominator = denominator / factor;
      this._numerator = numerator / factor;
    } else {
      this._denominator = 1;
      this._numerator = decimalArray[0];
    }
  }

  get decimal() {
    return this._decimal;
  }

  get numerator() {
    return this._numerator;
  }

  get denominator() {
    return this._denominator;
  }


  _highestCommonFactor(a,b) {
      if (b==0) return a;
      return this._highestCommonFactor(b,a%b);
  }

  toString() {
    if (this._denominator === 1) {
      return this._numerator;
    } else if (this._numerator > this._denominator) {
      const dividend = Math.floor(this._numerator / this._denominator);
      const remainder = this._numerator % this._denominator;
      return dividend + ' ' + remainder + '/' + this._denominator;
    } else {
      return this._numerator + '/' + this._denominator;
    }
  }

}
;class Ingredient {
  constructor(name, quantity, method, id) {
    this._id = id || 0;
    this._name = name || '';
    this._method = method || '';
    this._quantity = quantity || new Quantity();
  }

  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get method() {
    return this._method;
  }

  set method(method) {
    this._method = method;
  }

  get quantity() {
    return this._quantity;
  }

  set quantity(quantity) {
    this._quantity = quantity;
  }

  scale(factor) {
    this._quantity.scale(factor);
    return this;
  }

  toString() {
    return this._quantity + ' ' + this._name;
  }

}
;class Quantity {
  constructor(quantity, unit) {
    this._quantity = quantity || 0;
    this._fraction = new Fraction(this._quantity);
    this._unit = unit && unit.name ? unit : (unit ? UnitMatcher.match(unit) : UnitMatcher.match('ea'));
  }

  get quantity() {
    return this._fraction;
  }

  set quantity(quantity) {
    let newQuantity = {};
    if (quantity) {
      if (quantity.includes) {
        if (quantity.includes('/')) {
          const fraction = quantity.split('/');
          if (fraction.length !==2) {
            newQuantity._quantity = void 0;
          } else {
            const decimal = parseFloat(fraction[0]) / parseFloat(fraction[1]);
            newQuantity._quantity = decimal;
            newQuantity._fraction = new Fraction(decimal);
          }
        } else {
          newQuantity._quantity = parseFloat(quantity);
          newQuantity._fraction = new Fraction(newQuantity._quantity);
        }
      } else if (quantity.denominator) {
        newQuantity._fraction = quantity;
        newQuantity._quantity = quantity.decimal;
      } else {
        newQuantity._quantity = parseFloat(quantity);
        this._fraction = new Fraction(this._quantity);
      }
    }

    if (newQuantity._quantity !== undefined && newQuantity._quantity !== NaN) {
      this._quantity = newQuantity._quantity;
      this._fraction = newQuantity._fraction;
    }
  }

  get unit() {
    return this._unit;
  }

  set unit(unit) {
    this._unit = unit;
  }

  simplify() {
    let newQuantity = new Quantity(this._quantity, this._unit);
    console.log('simplify: ', this);
    if (this._quantity < 1) {
      // simplify down
      while (newQuantity.quantity.decimal < 1 && newQuantity.unit.prev) {
        const unit = UnitMatcher.match(newQuantity.unit.prev);
        const quantity = Math.round(newQuantity.quantity.decimal * (newQuantity.unit.factor / unit.factor)*100000)/100000;
        if ('123468'.includes(new Fraction(quantity).denominator + '')) {
          newQuantity = new Quantity(quantity, unit);
        } else {
          break;
        }
      }
    } else {
      // simplify up
      while (newQuantity.unit.next && newQuantity.unit.next.length > 0) {
        const unit = UnitMatcher.match(newQuantity.unit.next);
        const quantity = Math.round(newQuantity.quantity.decimal * (newQuantity.unit.factor / unit.factor)*100000)/100000;
        const remainder = quantity % .25;
        if (remainder === 0) {
          newQuantity = new Quantity(quantity, unit);
        } else {
          break;
        }
      }
    }

    this.quantity = newQuantity.quantity;
    this.unit = newQuantity.unit;
    return this;
  }

  scale(factor) {
    this._quantity *= factor;
    this._fraction = new Fraction(this._quantity);
    this.simplify();
    return this;
  }

  toDecimalString() {
    return this._quantity + ' ' + this._unit.name + this._pluralize();
  }

  toString() {
    return new Fraction(this._quantity) + ' ' + this._unit.name + this._pluralize();
  }

  _pluralize() {
    return (this._quantity > 1 && this._unit.type > 1 ? 's' : '');
  }
}
;class Recipe {
  constructor(description, name, rYield, ingredients) {
    this._description = description || '';
    this._name = name || '';
    this._yield = rYield || new Quantity(0, 'ea');

    if (ingredients && ingredients.length > 0) {
      const ids = ingredients.map(ingredient => ingredient.id);
      const lastId = Math.max(...ids);
      this._ingredients = ingredients;
      this._nextId = lastId + 1;
    } else {
      this._ingredients = [];
      this._nextId = 0;
    }
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get yield() {
    return this._yield;
  }

  set yield(rYield) {
    this._yield = rYield;
  }

  get ingredients() {
    return this._ingredients;
  }

  set ingredients(ingredients) {
    this._ingredients = ingredients;
  }

  addIngredient(ingredient) {
    if (ingredient) {
      ingredient.id = this._nextId;
      ingredient.quantity.simplify();
      this._nextId++;
      this._ingredients.push(ingredient);
    }
  }

  scale(factor) {
    if (factor > 0) {
      this._yield.scale(factor);
      this._ingredients.forEach(i => i.scale(factor));
    }
  }
}
