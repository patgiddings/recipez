angular.module('recipez').directive('slide', [function() {
  return {
    link: function(scope, element, attrs) {
      element._position = 0;
      function slide(position) {
          element.removeClass('pos-' + element._position);
          element._position = position;
          element.addClass('pos-' + position);
      }
      scope.onSlide(slide);
    }
  }
}])
.directive('formGroup', ['$parse', function($parse) {
  let template =
    "<div class='form_group'>" +
      "<label for='{{name | lowercase}}' ng-click='toggle($event);'>{{name}}</label>" +
      "<input type='text' name='{{name | lowercase}}' ng-model='innerModel' ng-focus='focus($event);' ng-blur='focus($event, true);' ng-keypress='checkForEnter($event);'>" +
    "</div>";

  function link(scope, element, attrs) {
    scope.innerModel = scope.model || '';
    scope.$watch('model', (m) => { scope.innerModel = m || '';});
    if ('down' in attrs) {
      element.addClass('down');
    }
    scope.focus = function(e, blur) {
      const el = angular.element(e.target);
      if (blur) {
        scope.model = scope.innerModel;
        el.removeClass('focused');
        el.prev().removeClass('focused');
        ('down' in attrs) && el.prev().removeClass('down');
        (scope.onBlur) && scope.onBlur({qty: scope.model});
      } else {
        el.addClass('focused');
        el.prev().addClass('focused');
        ('down' in attrs) && el.prev().addClass('down');
      }
    };
    scope.checkForEnter = function(e) {
      if (e.which === 13) {
        angular.element(e.target).blur();
        if ('onEnter' in attrs) {
          scope.onEnter();
        }
      }
    };
    scope.toggle = function(e) {
      const el = angular.element(e.target).next();
      if (el.hasClass('focused')) {
        el.blur();
      } else {
        el.focus();
      }
    }
  }

  return {
    restrict: 'E',
    replace: true,
    transclude: false,
    scope: {
      model: "=",
      name: "@",
      onEnter: "&?",
      onBlur: "&?",
      noShadow: "=?"
    },
    link: link,
    template: template
  };
}])
