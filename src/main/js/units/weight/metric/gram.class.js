class Gram extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'gram',
      abbrev: 'g',
      matcher: /\bg\b|gram/,
      factor: 1
    });
  }
}
