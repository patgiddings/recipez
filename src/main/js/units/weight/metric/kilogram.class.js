class Kilogram extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'kilogram',
      abbrev: 'kg',
      matcher: /\bkg\b|kilo\b|kilogram/,
      factor: 1000
    });
  }
}
