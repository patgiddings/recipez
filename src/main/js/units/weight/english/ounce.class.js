class Ounce extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'ounce',
      abbrev: 'oz',
      matcher: /\boz\b|\boz\.\b|ounce/,
      factor: 28.349523125,
      next: 'lb'
    });
  }
}
