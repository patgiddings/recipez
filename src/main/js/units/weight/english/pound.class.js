class Pound extends Unit {
  constructor() {
    super({
      type: 2,
      name: 'pound',
      abbrev: 'lb',
      matcher: /\blb\b|\blb\.\b|pound/,
      factor: 453.59237,
      prev: 'oz'
    });
  }
}
