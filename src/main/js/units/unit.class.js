class Unit {
  constructor(init) {
    // type can be: 0->unassigned, 1->count, 2->weight, 3->volume
    this._type = init && init.type ? init.type : 0;
    this._name = init && init.name ? init.name : 'Unassigned';
    this._abbrev = init && init.abbrev ? init.abbrev : '';
    this._matcher = init && init.matcher ? init.matcher : /\w\b\w/;
    // factor is in relation to SI base unit (ea, g, ml)
    this._factor = init && init.factor ? init.factor : 1;
    this._next = init && init.next ? init.next : '';
    this._prev = init && init.prev ? init.prev : '';
  }

  get type() {
    return this._type;
  }

  set type(type) {
    this._type = type;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    const unit = UnitMatcher.match(name);
    if (unit) {
      this._copy(unit);
    }
  }

  get abbrev() {
    return this._abbrev;
  }

  set abbrev(abbrev) {
    this._abbrev = abbrev;
  }

  get matcher() {
    return this._matcher;
  }

  set matcher(matcher) {
    this._matcher = matcher;
  }

  get factor() {
    return this._factor;
  }

  set factor(factor) {
    this._factor = factor;
  }

  get next() {
    return this._next;
  }

  set next(next) {
    this._next = next;
  }

  get prev() {
    return this._prev;
  }

  set prev(prev) {
    this._prev = prev;
  }

  _copy(b) {
    this._type = b.type;
    this._name = b.name;
    this._abbrev = b.abbrev;
    this._matcher = b.matcher;
    this._factor = b.factor;
    this._next = b.next;
    this._prev = b.prev;
  }
}
