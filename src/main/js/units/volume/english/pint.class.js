class Pint extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'pint',
      abbrev: 'pt',
      matcher: /\bpt\b|pint/,
      factor: 473.176473,
      next: 'qt',
      prev: 'c'
    });
  }
}
