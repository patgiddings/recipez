class Quart extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'quart',
      abbrev: 'qt',
      matcher: /\bqt\b|quart/,
      factor: 946.352946,
      next: 'gal',
      prev: 'pt'
    });
  }
}
