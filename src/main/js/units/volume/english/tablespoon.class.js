class Tablespoon extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'tablespoon',
      abbrev: 'tbl',
      matcher: /tbl|tablespoon|\btb\b/,
      factor: 14.78676478125,
      next: 'cup',
      prev: 'tsp'
    });
  }
}
