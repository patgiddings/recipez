class FluidOunce extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'fluid ounce',
      abbrev: 'fl oz',
      matcher: /fl\.\soz|fluid\sounce|fluid\soz|fl\soz/,
      factor: 29.5735295625,
      next: 'cup',
      prev: 'tbl'
    });
  }
}
