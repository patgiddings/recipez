class Teaspoon extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'teaspoon',
      abbrev: 'tsp',
      matcher: /tsp|teaspoon|\bts\b/,
      factor: 4.92892159375,
      next: 'tbl'
    });
  }
}
