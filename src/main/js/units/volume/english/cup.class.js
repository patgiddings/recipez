class Cup extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'cup',
      abbrev: 'c',
      matcher: /\bc\b|cup|\bcp\b/,
      factor: 236.5882365,
      next: 'pt',
      prev: 'fl oz'
    });
  }
}
