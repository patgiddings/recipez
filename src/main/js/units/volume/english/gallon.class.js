class Gallon extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'gallon',
      abbrev: 'gal',
      matcher: /gal|gallon/,
      factor: 3785.411784,
      prev: 'qt'
    });
  }
}
