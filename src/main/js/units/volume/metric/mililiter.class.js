class Mililiter extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'mililiter',
      abbrev: 'ml',
      matcher: /\bml\|mililiter|mili\b/,
      factor: 1,
      next: 'lt'
    });
  }
}
