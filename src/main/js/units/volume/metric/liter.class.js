class Liter extends Unit {
  constructor() {
    super({
      type: 3,
      name: 'liter',
      abbrev: 'lt',
      matcher: /\blt\b|liter|\bl\b/,
      factor: 1000,
      prev: 'ml'
    });
  }
}
