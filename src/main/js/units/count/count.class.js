class Count extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'count',
      abbrev: 'ct',
      matcher: /\bct\b|\bct\.\b|count/,
      factor: 1,
      next: 'dz'
    });
  }
}
