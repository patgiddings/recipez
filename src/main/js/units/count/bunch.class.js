class Bunch extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'bunch',
      abbrev: 'bu',
      matcher: /\bbu\b|bunch/,
      factor: 1
    });
  }
}
