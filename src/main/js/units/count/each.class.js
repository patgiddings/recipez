class Each extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'each',
      abbrev: 'ea',
      matcher: /\bea\b|\bea\.\b|each/,
      factor: 1,
      next: 'dz'
    });
  }
}
