class Dozen extends Unit {
  constructor() {
    super({
      type: 1,
      name: 'dozen',
      abbrev: 'dz',
      matcher: /\bdz\b|dozen/,
      factor: 12,
      prev: 'ct'
    });
  }
}
