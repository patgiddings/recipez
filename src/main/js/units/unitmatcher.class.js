class UnitMatcher {
  static get units() {
    return [
      new Each(),
      new Count(),
      new Bunch(),
      new Dozen(),
      new FluidOunce(),
      new Teaspoon(),
      new Tablespoon(),
      new Cup(),
      new Pint(),
      new Quart(),
      new Gallon(),
      new Ounce(),
      new Pound(),
      new Mililiter(),
      new Liter(),
      new Gram(),
      new Kilogram()
    ];
  }

  static match(unit) {
    let units = UnitMatcher.units;
    for (let i = 0; i < units.length; i++) {
      if (unit.search(units[i].matcher) >= 0) {
        return units[i];
      }
    }
  }

}
