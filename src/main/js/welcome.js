angular.module('recipez', [])
.controller('welcome', ['$scope', '$timeout', function($scope, $timeout) {
  var self = this;

  self.positionCount = 2;
  self.slideListeners = [];
  self.about = ['What is Recipease?', 'Tell me about the recipe.', 'What goes into it?'];
  self.recipe = new Recipe(
    'Fresh salsa',
    'Pico de Gallo',
    new Quantity(3, 'qt'),
    [
      new Ingredient('Lime', new Quantity(2, 'ea'), 'juice and zest', 0),
      new Ingredient('Cilantro', new Quantity(.5, 'bu'), 'chopped', 10)
    ]
  );
  // self.recipe = new Recipe();

  self.ingredient = new Ingredient();

  $scope.state = {
    position: 2,
    positions: 4
  };

  $scope.onSlide = function(listener) {
    self.slideListeners.push(listener);
    listener($scope.state.position);
  }

  self.notify = function() {
    self.slideListeners.forEach((f) => { f($scope.state.position); });
  }

  self.slideUp = function() {
    if ($scope.state.position >= $scope.state.positions - 1) {
      return;
    }

    $scope.state.position++;
    self.notify();
  }

  self.slideDown = function() {
    if ($scope.state.position <= 0) {
      return;
    }

    $scope.state.position--;
    self.notify();
  }

  self.addIngredient = function() {
    angular.element('#ingredient-name :input  ')[0].focus();
    if (self.ingredient.name.length === 0) {
      return;
    }
    $timeout(function() {
      self.recipe.addIngredient(self.ingredient);
      self.ingredient = new Ingredient();
    }, 200);
  }

  self.parseQuantity = function(qty) {
    // if (qty && qty.includes) {
    //   if (qty.includes('/')) {
    //     const fraction = qty.split('/');
    //     if (fraction.length !== 2) {
    //       self.ingredient.quantity.quantity = 0;
    //     } else {
    //       const decimal = parseFloat(fraction[0]) / parseFloat(fraction[1]);
    //       self.ingredient.quantity.quantity = decimal;
    //     }
    //   } else {
    //     self.ingredient.quantity.quantity = parseFloat(qty);
    //   }
    // } else {
    //   self.ingredient.quantity.quantity = 0;
    // }
  }

  self.popIngredient = function(index) {
    self.ingredient = self.recipe.ingredients[index];
    self.recipe.ingredients.splice(index, 1);
  }

  self.scale = function() {
    self.recipe.scale(.5);
  }

  self.submit = function() {
    console.log(['Submit Recipe', self.recipe]);
  }

}]);
